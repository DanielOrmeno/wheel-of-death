using System;
namespace LinkedListDS {
    public class WheelOfDeath
    {
        private Node head {get; set;}
        public Node Head => head;

        public WheelOfDeath(int numberOfVictims)
        {
            for (var i = 1; i<=numberOfVictims; i++)
            {
                InsertAtTail(i);
            }

            GetTail().Next = head;
        }

        public void StartMayhem(bool showBloodBath)
        {
            var dude = head;
            while (dude.Next != null)
            {
                CommitMurder(dude, showBloodBath);

                if (dude.Next != null)
                    dude = dude.Next;
                
                if (dude.Next == dude)
                    break;
            }

            Console.WriteLine($"\nOnly #{dude.Value} survived");
        }

        public void CommitMurder(Node thisNode, bool showBloodBath)
        {
            if (thisNode.Next != null && thisNode.Next != thisNode)
            {
                if (showBloodBath)
                {
                    Console.OutputEncoding = System.Text.Encoding.UTF8;
                    Console.WriteLine($"{thisNode.Value} Kills {thisNode.Next.Value} , Now {thisNode.Next.Next.Value} now has the Sword 💁");
                }

                thisNode.Next = thisNode.Next.Next;
            }
        }

        public Node GetTail()
        {
            if (head == null) return null;

            var tmp = head;
            while (tmp.Next != null)
            {
                tmp = tmp.Next;
            }

            return tmp;
        }
        public void InsertAtTail(int val)
        {
            if (head == null)
            {
                head = new Node(val);
            }
            else
            {
                var tmp = head;
                while (tmp.Next != null)
                {
                    tmp = tmp.Next;
                }
                tmp.Next = new Node(val);
            }
        }

        public void InsertAtHead(int val)
        {
            if (head == null)
            {
                head = new Node(val);
            }
            else 
            {
                var tmp = new Node(val);
                tmp.Next = head;
                head = tmp;
            }
        }

        public void Print()
        {
            var tmp = head;
            var list = string.Empty;
            
            while (tmp != null)
            {
                list = list.Equals(string.Empty) ? tmp.Value.ToString() : string.Concat(list,$" -> {tmp.Value}");
                tmp = tmp.Next;
            }

            Console.WriteLine(list);
        }
    }
}
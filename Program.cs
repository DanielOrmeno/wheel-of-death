﻿using LinkedListDS;
using System.IO;
using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            var WheelOfDeath = new WheelOfDeath(GetValidInput());
            WheelOfDeath.StartMayhem(true);
        }

        public static void PrintLogo()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            var logo = File.ReadAllText("./Logo.txt");
            if (!string.IsNullOrEmpty(logo))
                Console.WriteLine(logo);
            Console.ResetColor();
        }

        public static int GetValidInput()
        {
            var shouldExit = false;
            while (!shouldExit)
            {
                PrintLogo();
                Console.WriteLine("\n\nEnter number of victims");
                try
                {
                    return Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return 0;
        }
    }
}
